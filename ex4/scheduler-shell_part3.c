#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <assert.h>

#include <sys/wait.h>
#include <sys/types.h>

#include "proc-common.h"
#include "request.h"

/* Compile-time parameters. */
#define SCHED_TQ_SEC 2                /* time quantum */
#define TASK_NAME_SZ 60               /* maximum size for a task's name */
#define SHELL_EXECUTABLE_NAME "shell" /* executable for shell */
struct PCB{
	pid_t pid;//process pid
	int num;//process number
	char task_name[TASK_NAME_SZ];//name of the process
	struct PCB* next;
	};

typedef struct PCB PCB;
PCB *Current,*temp,*Q;//o temp dixni tn proigumeni diadikasia-node  p imastan
int AliveProcesses,NoProcesses;
//PCB *Q;
//Q=(PCB*)malloc(sizeof(PCB));


/* Print a list of all tasks currently being scheduled.  */
static void
sched_print_tasks(void)
{
	PCB *list=Current;
	printf("ID = %d, PID = %d, name = %s\n", list->num,list->pid,list->task_name);
	list=list->next;
	while(list->num!=Current->num){
		printf("ID = %d, PID = %d, name = %s\n", list->num,list->pid,list->task_name);
		list=list->next;
	}
	printf("the current process name = %s , id = %d, pid = %d \n",Current->task_name,Current->num,Current->pid);

}

/* Send SIGKILL to a task determined by the value of its
 * scheduler-specific id.
 */
static int
sched_kill_task_by_id(int id)
{	
	PCB *list=Q->next;
	
	if(Q->num==id){ 
		kill(Q->pid,SIGTERM);
				
		printf("the process with id=%d and pid= %d and name=%s has been terminated by shell\n",Q->num,Q->pid,Q->task_name);
		
		return; 
	}
	
	while(list->num!=Q->num){
			if(list->num==id) {
				kill(list->pid,SIGTERM);
				
				printf("the process with id=%d and pid= %d and name=%s has been terminated by shell\n",list->num,list->pid,list->task_name);
				
				
				return;
			}
		list=list->next;
		
	
	}
		printf("the process with id=%d hasn't been found in our scheduler\n",id);
	return	0;// -ENOSYS;
}


/* Create a new task.  */
static void
sched_create_task(char *executable)
{	
	AliveProcesses++;
	pid_t p;
	PCB *t;
	p=fork();
	NoProcesses++;
	if(p<0)
		perror("error in fork");
	if(p==0)
		child(executable);
	if(p>0)	{//parents process
		t=(PCB*)malloc(sizeof(PCB));
		t->pid=p;
		t->num=NoProcesses;
		strcpy(t->task_name,executable);
		temp->next=t;
		t->next=Current;
		temp=temp->next;

	}
	
}

/* Process requests by the shell.  */
static int
process_request(struct request_struct *rq)
{
	switch (rq->request_no) {
		case REQ_PRINT_TASKS:
			sched_print_tasks();
			return 0;

		case REQ_KILL_TASK:
			return sched_kill_task_by_id(rq->task_arg);

		case REQ_EXEC_TASK:
			sched_create_task(rq->exec_task_arg);
			return 0;

		default:
			return -ENOSYS;
	}
}

/* 
 * SIGALRM handler
 */
static void
sigalrm_handler(int signum)
{
	//-> original written in here ->assert(0 && "Please fill me!");
	int p=kill(Current->pid,SIGSTOP);
	//	if(AliveProcesses!=1)//an ine mono mia diergasia,tote kane tin mexri na teliosi,xoris timer ktlp 
	if(p < 0){
		perror("SIGSTOP ERROR\n");	//alios stile stin energi tora diergasia sigstop na stamatisi
		exit(1);}
	printf("Scheduler: Received SIGALARM,process with PID=%d,is stopped by SIGSTOP\n",Current->pid);
}

/* 
 * SIGCHLD handler
 */
static void
sigchld_handler(int signum)
{
	pid_t p;
	int status;
	
	for(;;){	
		p = waitpid(-1, &status, WUNTRACED | WNOHANG);
		if (p < 0) {
			perror("waitpid");
			exit(1);
		}
		if (p == 0)
			break;
	
		

		if (WIFEXITED(status) || WIFSIGNALED(status)) {
			/* A child has died */
			alarm(0);
			AliveProcesses--;
			if(AliveProcesses!=0){
			
			temp->next=Current->next;
			printf("Scheduler: Received SIGCHLD, child is dead. Exiting.\n");
			free(Current);
			
				Current=temp->next;
				kill(Current->pid,SIGCONT);
				if(AliveProcesses!=1)
					alarm(SCHED_TQ_SEC);

					printf("Scheduler: Starting new process with PID=%d\n",Current->pid);
					
			}
			else{			
				printf("Scheduler: no more processes are alive-> Exiting.\n");
				exit(1);
			}

			
		} 
		 if (WIFSTOPPED(status)) {
			/* A child has stopped due to SIGSTOP/SIGTSTP, etc... */
			//printf("Scheduler:Received SIGCHLD, child process with PID =%d has been stopped\n",Current->pid);	
			temp=Current;
			Current=Current->next;				
			kill(Current->pid,SIGCONT);
			alarm(SCHED_TQ_SEC);
			printf("Scheduler: Starting new process with PID=%d\n",Current->pid);
		
		}
	
}
}

/* Disable delivery of SIGALRM and SIGCHLD. */
static void
signals_disable(void)
{
	sigset_t sigset;

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGALRM);
	sigaddset(&sigset, SIGCHLD);
	if (sigprocmask(SIG_BLOCK, &sigset, NULL) < 0) {
		perror("signals_disable: sigprocmask");
		exit(1);
	}
}

/* Enable delivery of SIGALRM and SIGCHLD.  */
static void
signals_enable(void)
{
	sigset_t sigset;

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGALRM);
	sigaddset(&sigset, SIGCHLD);
	if (sigprocmask(SIG_UNBLOCK, &sigset, NULL) < 0) {
		perror("signals_enable: sigprocmask");
		exit(1);
	}
}


/* Install two signal handlers.
 * One for SIGCHLD, one for SIGALRM.
 * Make sure both signals are masked when one of them is running.
 */
static void
install_signal_handlers(void)
{
	sigset_t sigset;
	struct sigaction sa;

	sa.sa_handler = sigchld_handler;
	sa.sa_flags = SA_RESTART;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGCHLD);
	sigaddset(&sigset, SIGALRM);
	sa.sa_mask = sigset;
	if (sigaction(SIGCHLD, &sa, NULL) < 0) {
		perror("sigaction: sigchld");
		exit(1);
	}

	sa.sa_handler = sigalrm_handler;
	if (sigaction(SIGALRM, &sa, NULL) < 0) {
		perror("sigaction: sigalrm");
		exit(1);
	}

	/*
	 * Ignore SIGPIPE, so that write()s to pipes
	 * with no reader do not result in us being killed,
	 * and write() returns EPIPE instead.
	 */
	if (signal(SIGPIPE, SIG_IGN) < 0) {
		perror("signal: sigpipe");
		exit(1);
	}
}

static void
do_shell(char *executable, int wfd, int rfd)
{
	char arg1[10], arg2[10];
	char *newargv[] = { executable, NULL, NULL, NULL };
	char *newenviron[] = { NULL };

	sprintf(arg1, "%05d", wfd);
	sprintf(arg2, "%05d", rfd);
	newargv[1] = arg1;
	newargv[2] = arg2;

	raise(SIGSTOP);
	execve(executable, newargv, newenviron);

	/* execve() only returns on error */
	perror("scheduler: child: execve");
	exit(1);
}

/* Create a new shell task.
 *
 * The shell gets special treatment:
 * two pipes are created for communication and passed
 * as command-line arguments to the executable.
 */
static void
sched_create_shell(char *executable, int *request_fd, int *return_fd)
{
	pid_t p;
	int pfds_rq[2], pfds_ret[2];

	if (pipe(pfds_rq) < 0 || pipe(pfds_ret) < 0) {
		perror("pipe");
		exit(1);
	}

	p = fork();
	PCB *shellBlock;
	shellBlock=(PCB*)malloc(sizeof(PCB));
				shellBlock->pid=p;
				shellBlock->num=1;
				strcpy(shellBlock->task_name,executable);
				shellBlock->next=NULL;
				Q=shellBlock;
				Current=Q;
	
	if (p < 0) {
		perror("scheduler: fork");
		exit(1);
	}

	if (p == 0) {
		/* Child */
		close(pfds_rq[0]);
		close(pfds_ret[1]);
		do_shell(executable, pfds_rq[1], pfds_ret[0]);
		assert(0);
	}
	/* Parent */
	close(pfds_rq[1]);
	close(pfds_ret[0]);
	*request_fd = pfds_rq[0];
	*return_fd = pfds_ret[1];
}

static void
shell_request_loop(int request_fd, int return_fd)
{
	int ret;
	struct request_struct rq;

	/*
	 * Keep receiving requests from the shell.
	 */
	for (;;) {
		if (read(request_fd, &rq, sizeof(rq)) != sizeof(rq)) {
			perror("scheduler: read from shell");
			fprintf(stderr, "Scheduler: giving up on shell request processing.\n");
			break;
		}

		signals_disable();
		ret = process_request(&rq);
		signals_enable();

		if (write(return_fd, &ret, sizeof(ret)) != sizeof(ret)) {
			perror("scheduler: write to shell");
			fprintf(stderr, "Scheduler: giving up on shell request processing.\n");
			break;
		}
	}
}



void child(char *task_name){
	char *newargv[] = { task_name, NULL, NULL, NULL };
	char *newenviron[] = { NULL };

	printf("I am %s, we are in the child's process with PID = %ld\n",
		task_name, (long)getpid());
	raise(SIGSTOP);


	execve(task_name, newargv, newenviron);

	/* execve() only returns on error */
	perror("execve");
	exit(1);

}


int main(int argc, char *argv[])
{
	 int nproc;int i;
	pid_t pid;
	//PCB *Q=NULL;
	/* Two file descriptors for communication with the shell */
	static int request_fd, return_fd;

	/* Create the shell. */
	sched_create_shell(SHELL_EXECUTABLE_NAME, &request_fd, &return_fd);
	/* TODO: add the shell to the scheduler's tasks */

	/*
	 * For each of argv[1] to argv[argc - 1],
	 * create a new child process, add it to the process list.
	 */

	nproc = argc-1; /* number of proccesses goes here */

	AliveProcesses=nproc+1;
	NoProcesses=nproc+1;
	

	for(i=0;i<nproc;i++){	
			while(Q==NULL)
				;
			pid=fork();
			if(pid==0){	//if we are in child's process call the child function.
				child(argv[i+1]);
				exit(1);
			}
			if (pid==-1)
				perror("error in creating child process\n");
			if(pid>0){
				
					
						;
				temp=(PCB*)malloc(sizeof(PCB));
				temp->pid=pid;
				temp->num=i+2;
				strcpy(temp->task_name,argv[i+1]);
	
				if(i==nproc-1)
					temp->next=Q;//to create the round robin we create a circular linked list
				else 
					temp->next=NULL;
				
				Current->next=temp;
				Current=temp;
				}
			}					
	

	/* Wait for all children to raise SIGSTOP before exec()ing. */
	wait_for_ready_children(nproc+1);

	/* Install SIGALRM and SIGCHLD handlers. */
	install_signal_handlers();

	if (nproc == 0) {
		fprintf(stderr, "Scheduler: No tasks. Exiting...\n");
		exit(1);
	}
	Current=Q;
	kill(Current->pid,SIGCONT);
	if(AliveProcesses!=1)
		alarm(SCHED_TQ_SEC);
	printf("Scheduler: Beginning scheduling\n");

	shell_request_loop(request_fd, return_fd);

	/* Now that the shell is gone, just loop forever
	 * until we exit from inside a signal handler.
	 */
	while (pause())
		;

	/* Unreachable */
	fprintf(stderr, "Internal error: Reached unreachable point\n");
	return 1;
}
