#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <assert.h>

#include <sys/wait.h>
#include <sys/types.h>

#include "proc-common.h"
#include "request.h"

/* Compile-time parameters. */
#define SCHED_TQ_SEC 2                /* time quantum */
#define TASK_NAME_SZ 60               /* maximum size for a task's name */
#define SHELL_EXECUTABLE_NAME "shell" /* executable for shell */


struct PCB{
	pid_t pid;//process pid
	int num;//process number
	char task_name[TASK_NAME_SZ];//name of the process
	struct PCB* next;
	int priority; //high=1 ,low =0; 
	};

typedef struct PCB PCB;
PCB *Current, *Q, *CurrentH, *CurrentL, *prevH, *prevL, *temp, *Qh, *Ql;//o temp dixni tn proigumeni diadikasia-node  p imastan
int AliveProcesses,NoProcesses,PR;



static void sched_to_LOW(int id){

	PCB *CurrentHH;
	
	if(CurrentH!=NULL) {
		
		if(CurrentH->num==id){ 
			//printf("BBBBBIIIIKA currentHHHHH\n");
			CurrentH->priority=0;	
			//free CurrentL from low list
			prevH->next = CurrentH->next;	
			
			//move CurrentL PCB to high list
			if(CurrentL==NULL){
				CurrentL=CurrentH;
				CurrentL->next=CurrentL;
				prevL = CurrentL;
				prevL->next=CurrentL;
				CurrentHH=CurrentH;
			}
			else{
				//printf("BBBBBIIIIKA elseeeeeeeeeeeeeeeeeTOLOW\n");
				prevL->next = CurrentH;
				CurrentH->next = CurrentL;
				prevL = CurrentH;
				CurrentHH=CurrentH;
			}

		
			if(CurrentH==prevH){///////////////
				//printf("BBBBBIIIIKA KAI STO CH==PRVHHH\n");
				CurrentH=NULL;
				prevH=NULL;
			}else	CurrentH = prevH->next;

			printf("the process with id=%d and pid= %d and name=%s has changed its priority to LOW\n",
				CurrentHH->num,CurrentHH->pid,CurrentHH->task_name);		
		
			return; 
		}
		PCB *prevHH=CurrentH, *listH=CurrentH->next;
		while(listH->num!=CurrentH->num){
		
			if(listH->num==id) {
				listH->priority = 0;
				//free CurrentL from low list
				prevHH->next = listH->next;
				CurrentHH=listH;
				//move listL PCB to high list
				if(CurrentL==NULL){
					CurrentL=listH;
					CurrentL->next=listH;
					prevL=CurrentL;
					prevL->next=CurrentL;
				}else{
			
					prevL->next = listH;
					listH->next = CurrentL;
					prevL = listH;	
			
				}
				/*if(listL==prevLL){
			
					prevL=NULL;
					CurrentL=NULL;
				}*/
				printf("the process with id=%d and pid= %d and name=%s has changed its priority to LOW\n",
				CurrentHH->num,CurrentHH->pid,CurrentHH->task_name);		
					
				
				return;
			}
			listH=listH->next;
			prevHH=prevHH->next;
		
		}
	}
	printf("the process with id=%d hasn't been found in HIGH list\n",id);
	return	0;
}
	


static void sched_to_HIGH(int id){
	PCB *CurrentLL;
	
	if(CurrentL!=NULL) {
		
		if(CurrentL->num==id){ 
			//printf("BBBBBIIIIKA currentLLLLLLL\n");
			CurrentL->priority=1;	
			//free CurrentL from low list
			prevL->next = CurrentL->next;	
			
			//move CurrentL PCB to high list
			if(CurrentH==NULL){
				CurrentH=CurrentL;
				CurrentH->next=CurrentH;
				prevH = CurrentH;
				prevH->next=CurrentH;
				CurrentLL=CurrentL;
			}
			else{
				//printf("BBBBBIIIIKA elseeeeeeeeeeeeeeeee\n");
				prevH->next = CurrentL;
				CurrentL->next = CurrentH;
				prevH = CurrentL;
				CurrentLL=CurrentL;
			}

		
			if(CurrentL==prevL){///////////////
				//printf("BBBBBIIIIKA KAI STO CL==PRVL\n");
				CurrentL=NULL;
				prevL=NULL;
			}else	CurrentL = prevL->next;

			printf("the process with id=%d and pid= %d and name=%s has changed its priority to HIGH\n",
				CurrentLL->num,CurrentLL->pid,CurrentLL->task_name);		
		
			return; 
		}
		PCB *prevLL=CurrentL, *listL=CurrentL->next;
		while(listL->num!=CurrentL->num){
			if (listL->next == prevLL) { //an exoun meinei 2 stoixeia
				if(listL->num==id) {
					listL->priority = 1;
					CurrentL->next = CurrentL;

					if(CurrentH==NULL){
					CurrentH=listL;
					CurrentH->next=listL;
					prevH=CurrentH;
					prevH->next=CurrentH;
				}else{
					prevH->next = listL;
					listL->next = CurrentH;
					prevH = listL;	
				}
					

					listL=CurrentL;
					CurrentLL=listL;
				}
			} else if(listL->num==id) {
				listL->priority = 1;
				//free CurrentL from low list
				prevLL->next = listL->next;
				CurrentLL=listL;
				//move listL PCB to high list
				if(CurrentH==NULL){
					CurrentH=listL;
					CurrentH->next=listL;
					prevH=CurrentH;
					prevH->next=CurrentH;
				}else{
			
					prevH->next = listL;
					listL->next = CurrentH;
					prevH = listL;	
			
				}
				/*if(listL==prevLL){
			
					prevL=NULL;
					CurrentL=NULL;
				}*/
				printf("the process with id=%d and pid= %d and name=%s has changed its priority to HIGH\n",
				CurrentLL->num,CurrentLL->pid,CurrentLL->task_name);		
					
				
				return;
			}
			listL=listL->next;
			prevLL=prevLL->next;
		
		}
	}
	printf("the process with id=%d hasn't been found in LOW list\n",id);
	return	0;
}





/* Print a list of all tasks currently being scheduled.  */
static void
sched_print_tasks(void)
{
	
	
	
	if (CurrentH != NULL) {
		printf("Processes with HIGH priority:\n");
		printf("ID = %d, PID = %d, name = %s, priority = %d\n", 
			CurrentH->num,CurrentH->pid,CurrentH->task_name,CurrentH->priority);
		PCB *listH=CurrentH;
		listH=listH->next;
		while(listH->num!=CurrentH->num){
			printf("ID = %d, PID = %d, name = %s, priority = %d\n", 
				listH->num,listH->pid,listH->task_name,listH->priority);
			listH=listH->next;
		}
	}
	
	if (CurrentL != NULL) {
		printf("Processes with LOW priority:\n");
		printf("ID = %d, PID = %d, name = %s, priority = %d\n", 
			CurrentL->num,CurrentL->pid,CurrentL->task_name,CurrentL->priority);
		PCB *listL=CurrentL;
		listL=listL->next;
		while(listL->num!=CurrentL->num){
			printf("ID = %d, PID = %d, name = %s, priority = %d\n", 
				listL->num,listL->pid,listL->task_name,listL->priority);
			listL=listL->next;
		}
	}
	printf("The current process name = %s , id = %d, pid = %d, priority = %d\n",
		Current->task_name,Current->num,Current->pid,Current->priority);
}

/* Send SIGKILL to a task determined by the value of its
 * scheduler-specific id.
 */
static int
sched_kill_task_by_id(int id)
{	
	
	if (CurrentL != NULL) {	//search in LOW list for proc with id=id
		
		if(CurrentL->num==id){ 
			Current=CurrentL;temp=prevL;
			kill(CurrentL->pid,SIGKILL);		
			printf("the process with id=%d and pid= %d and name=%s and priority=%d(l) has been terminated by shell\n",
				CurrentL->num,CurrentL->pid,CurrentL->task_name, CurrentL->priority);	
			return; 
		}
		PCB *listL=CurrentL->next;
		PCB *ll=CurrentL;
		while(listL->num!=CurrentL->num){
			if(listL->num==id) {
				Current=listL;temp=ll;
				kill(listL->pid,SIGKILL);
				printf("the process with id=%d and pid= %d and name=%s and priority=%d(l) has been terminated by shell\n",
					listL->num,listL->pid,listL->task_name, listL->priority);				
				return;
				}
			listL=listL->next;ll=ll->next;	
	
		}
	}
	
	if (CurrentH != NULL) {	//search in HIGH list for proc with id=id

	if(CurrentH->num==id){ Current=CurrentH;temp=prevH;
		kill(CurrentH->pid,SIGTERM);		
		printf("the process with id=%d and pid= %d and name=%s and priority=%d(h) has been terminated by shell\n",
			CurrentH->num,CurrentH->pid,CurrentH->task_name, CurrentH->priority);	
		return; 
	}
	PCB *listH=CurrentH->next;
	PCB *hh=CurrentH;
	while(listH->num!=CurrentH->num){
		if(listH->num==id) {Current=listH;temp=hh;
			kill(listH->pid,SIGTERM);
			printf("the process with id=%d and pid= %d and name=%s and priority=%d(h) has been terminated by shell\n",
				listH->num,listH->pid,listH->task_name, listH->priority);				
				return;
			}
		listH=listH->next;	hh=hh->next;
	
	}
	}
	printf("the process with id=%d hasn't been found in our scheduler\n",id);
	return	0;// -ENOSYS;
}


/* Create a new task.  */
static void
sched_create_task(char *executable)
{	AliveProcesses++;
	pid_t p;
	PCB *t;
	p=fork();
	NoProcesses++;
	if(p<0)
		perror("error in fork");
	if(p==0)
		child(executable);
	if(p>0)	{//parents process
		t=(PCB*)malloc(sizeof(PCB));
		t->pid=p;
		t->num=NoProcesses;
		t->priority=0;
		strcpy(t->task_name,executable);
		if(prevL==NULL){

			prevL=t;prevL->next=t;CurrentL=t;CurrentL->next=t;}	
		else{
		prevL->next=t;
		t->next=CurrentL;		
		prevL=prevL->next;
		}

	}
	
	
}

/* Process requests by the shell.  */
static int
process_request(struct request_struct *rq)
{
	switch (rq->request_no) {
		case REQ_PRINT_TASKS:
			sched_print_tasks();
			return 0;

		case REQ_KILL_TASK:
			return sched_kill_task_by_id(rq->task_arg);

		case REQ_EXEC_TASK:
			sched_create_task(rq->exec_task_arg);
			return 0;

		case REQ_LOW_TASK:
			sched_to_LOW(rq->task_arg);
			return 0;

		case REQ_HIGH_TASK:
			sched_to_HIGH(rq->task_arg);
			return 0;

		default:
			return -ENOSYS;
	}
}

/* 
 * SIGALRM handler
 */
static void
sigalrm_handler(int signum)
{
	//-> original written in here ->assert(0 && "Please fill me!");
	int p=kill(Current->pid,SIGSTOP);
	//	if(AliveProcesses!=1)//an ine mono mia diergasia,tote kane tin mexri na teliosi,xoris timer ktlp 
	if(p < 0){
		perror("SIGSTOP ERROR\n");	//alios stile stin energi tora diergasia sigstop na stamatisi
		exit(1);}
	printf("Scheduler: Received SIGALARM, process with PID=%d is stopped by SIGSTOP\n",Current->pid);
}

/* 
 * SIGCHLD handler
 */
static void
sigchld_handler(int signum)
{
	pid_t p;
	int status;
	PCB *Cur, *prev;
	
	for(;;){	
		p = waitpid(-1, &status, WUNTRACED | WNOHANG);
		if (p < 0) {
			perror("waitpid");
			exit(1);
		}
		if (p == 0)
			break;
	
		if (WIFEXITED(status) || WIFSIGNALED(status)) {
		/* A child has died */
			if (p == Current->pid) {
				alarm(0);
				Cur = Current;
				prev = temp;
			} else {
				Cur = Current->next;
				prev = temp->next;
				while (Cur->num != Current->num) {
					Cur = Cur->next;
					prev = prev->next;
				}//by the end of this else, Cur will show to the PCB which caused sigchld(term/kill)
			}
			AliveProcesses--;
//printf("BIKAME STO SIGTERM\n");
			
			if(AliveProcesses!=0){	//Current-process has finished.
				int cr=Cur->priority;
				if (prev==Cur) {
//printf("BIKAME STO ALIVEPROC, TEMP=CURR\n");
					free(Cur);
					Cur = NULL;
					prev = NULL;
				} else {
//printf("BIKAME STO ALIVEPROC, TEMP!=CURR\n");
					prev->next=Cur->next;
					free(Cur);
					Cur=prev->next;
				}
				if(cr==0){
					CurrentL=Cur;
					prevL=prev;
				}
				else {
					CurrentH=Cur;
					prevH=prev;
				}
				printf("Scheduler: Received SIGCHLD, child is dead. Exiting.\n");
			
			//in every sgch_handler, if there is a process in High list, the following if-block
			//will be executed (scheduling th high prio processes). Only when this is empty, the 2nd
			//if-block will be executed.
				if (p == Current->pid) { //sigcont to next only if Current sent SIGSTERM/KILL.				
					if (CurrentH != NULL) { //if high list isn' t empty, continue in high list.
						Current = CurrentH;
						temp = prevH;
						kill(Current->pid,SIGCONT);
						alarm(SCHED_TQ_SEC);	
					}		
					else {
						if (CurrentL != NULL) { //when high list is empty, continue in low list.
							Current=CurrentL;
							temp = prevL;
							kill(Current->pid,SIGCONT);
							alarm(SCHED_TQ_SEC);	
						}
					}
					printf("Scheduler: Starting new process with PID=%d, %s\n",
						Current->pid,Current->task_name);		
				}			
			}
			else {			
				printf("Scheduler: no more processes are alive-> Exiting.\n");
				exit(1);
			}
			
		} 
		if (WIFSTOPPED(status)) {
			/* A child has stopped due to SIGSTOP/SIGTSTP, etc... */
			
			if (Current->pid == p) { //send sigcont only if sigstoped-cause-alarm process in here.
						 //other case for process in here: child() ( e prog).
				if (Current->priority==0){	//if we are in low list
					if (CurrentH!=NULL) {	//if a high node appears
						Current = CurrentH;
						temp = prevH;			
						kill(Current->pid,SIGCONT);
						alarm(SCHED_TQ_SEC);
						printf("Scheduler: Starting new process with PID=%d\n",Current->pid);
					} else {	//if no high node, continue in low list

						temp=Current;
						Current=Current->next;
						CurrentL = Current;
						prevL = temp;				
						kill(Current->pid,SIGCONT);
						alarm(SCHED_TQ_SEC);
						printf("Scheduler: Starting new process with PID=%d\n",Current->pid);
					}

				} else {	//else if in high list, stay here
					temp=Current;
					Current=Current->next;				
					kill(Current->pid,SIGCONT);
					alarm(SCHED_TQ_SEC);
					printf("Scheduler: Starting new process with PID=%d\n",Current->pid);
				}
			}
		}
	}
}


/* Disable delivery of SIGALRM and SIGCHLD. */
static void
signals_disable(void)
{
	sigset_t sigset;

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGALRM);
	sigaddset(&sigset, SIGCHLD);
	if (sigprocmask(SIG_BLOCK, &sigset, NULL) < 0) {
		perror("signals_disable: sigprocmask");
		exit(1);
	}
}

/* Enable delivery of SIGALRM and SIGCHLD.  */
static void
signals_enable(void)
{
	sigset_t sigset;

	sigemptyset(&sigset);
	sigaddset(&sigset, SIGALRM);
	sigaddset(&sigset, SIGCHLD);
	if (sigprocmask(SIG_UNBLOCK, &sigset, NULL) < 0) {
		perror("signals_enable: sigprocmask");
		exit(1);
	}
}


/* Install two signal handlers.
 * One for SIGCHLD, one for SIGALRM.
 * Make sure both signals are masked when one of them is running.
 */
static void
install_signal_handlers(void)
{
	sigset_t sigset;
	struct sigaction sa;

	sa.sa_handler = sigchld_handler;
	sa.sa_flags = SA_RESTART;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGCHLD);
	sigaddset(&sigset, SIGALRM);
	sa.sa_mask = sigset;
	if (sigaction(SIGCHLD, &sa, NULL) < 0) {
		perror("sigaction: sigchld");
		exit(1);
	}

	sa.sa_handler = sigalrm_handler;
	if (sigaction(SIGALRM, &sa, NULL) < 0) {
		perror("sigaction: sigalrm");
		exit(1);
	}

	/*
	 * Ignore SIGPIPE, so that write()s to pipes
	 * with no reader do not result in us being killed,
	 * and write() returns EPIPE instead.
	 */
	if (signal(SIGPIPE, SIG_IGN) < 0) {
		perror("signal: sigpipe");
		exit(1);
	}
}

static void
do_shell(char *executable, int wfd, int rfd)
{
	char arg1[10], arg2[10];
	char *newargv[] = { executable, NULL, NULL, NULL };
	char *newenviron[] = { NULL };

	sprintf(arg1, "%05d", wfd);
	sprintf(arg2, "%05d", rfd);
	newargv[1] = arg1;
	newargv[2] = arg2;

	raise(SIGSTOP);
	execve(executable, newargv, newenviron);

	/* execve() only returns on error */
	perror("scheduler: child: execve");
	exit(1);
}

/* Create a new shell task.
 *
 * The shell gets special treatment:
 * two pipes are created for communication and passed
 * as command-line arguments to the executable.
 */
static void
sched_create_shell(char *executable, int *request_fd, int *return_fd)
{
	pid_t p;
	int pfds_rq[2], pfds_ret[2];

	if (pipe(pfds_rq) < 0 || pipe(pfds_ret) < 0) {
		perror("pipe");
		exit(1);
	}

	p = fork();
	PCB *shellBlock;
	shellBlock=(PCB*)malloc(sizeof(PCB));
				shellBlock->pid=p;
				shellBlock->num=1;
				shellBlock->priority=0;
				strcpy(shellBlock->task_name,executable);
				shellBlock->next=shellBlock;
				Ql=shellBlock;
				CurrentL=Ql;
	
	if (p < 0) {
		perror("scheduler: fork");
		exit(1);
	}

	if (p == 0) {
		/* Child */
		close(pfds_rq[0]);
		close(pfds_ret[1]);
		do_shell(executable, pfds_rq[1], pfds_ret[0]);
		assert(0);
	}
	/* Parent */
	close(pfds_rq[1]);
	close(pfds_ret[0]);
	*request_fd = pfds_rq[0];
	*return_fd = pfds_ret[1];
}

static void
shell_request_loop(int request_fd, int return_fd)
{
	int ret;
	struct request_struct rq;

	/*
	 * Keep receiving requests from the shell.
	 */
	for (;;) {
		if (read(request_fd, &rq, sizeof(rq)) != sizeof(rq)) {
			perror("scheduler: read from shell");
			fprintf(stderr, "Scheduler: giving up on shell request processing.\n");
			break;
		}
		
		signals_disable();	
		ret = process_request(&rq);
		//CurrentL=prevL->next;	//den xreiazetai mallon
		signals_enable();

		if (write(return_fd, &ret, sizeof(ret)) != sizeof(ret)) {
			perror("scheduler: write to shell");
			fprintf(stderr, "Scheduler: giving up on shell request processing.\n");
			break;
		}
	}
}



void child(char *task_name){
	char *newargv[] = { task_name, NULL, NULL, NULL };
	char *newenviron[] = { NULL };

	printf("I am %s, we are in the child's process with PID = %ld\n",
		task_name, (long)getpid());
	raise(SIGSTOP);


	execve(task_name, newargv, newenviron);

	/* execve() only returns on error */
	perror("execve");
	exit(1);

}


int main(int argc, char *argv[])
{
	 int nproc;int i;
	pid_t pid;
	//PCB *Q=NULL;
	/* Two file descriptors for communication with the shell */
	static int request_fd, return_fd;

	/* Create the shell. */
	sched_create_shell(SHELL_EXECUTABLE_NAME, &request_fd, &return_fd);
	/* TODO: add the shell to the scheduler's tasks */

	/*
	 * For each of argv[1] to argv[argc - 1],
	 * create a new child process, add it to the process list.
	 */

	nproc = argc-1; /* number of proccesses goes here */

	AliveProcesses=nproc+1;
	NoProcesses=nproc+1;
	
	while(Ql==NULL)
		;
	for(i=0;i<nproc;i++){	
			
			pid=fork();
			if(pid==0){	//if we are in child's process call the child function.
				child(argv[i+1]);
				exit(1);
			}
			if (pid==-1)
				perror("error in creating child process\n");
			if(pid>0){
				
					
						
				prevL=(PCB*)malloc(sizeof(PCB));
				prevL->priority=0;
				prevL->pid=pid;
				prevL->num=i+2;
				strcpy(prevL->task_name,argv[i+1]);
	
				if(i==nproc-1)
					prevL->next=Ql;//to create the round robin we create a circular linked list
				else 
					prevL->next=NULL;
				
				CurrentL->next=prevL;
				CurrentL=prevL;
				}
			}					
	

	/* Wait for all children to raise SIGSTOP before exec()ing. */
	wait_for_ready_children(nproc+1);

	/* Install SIGALRM and SIGCHLD handlers. */
	install_signal_handlers();

	if (nproc == 0) {
		fprintf(stderr, "Scheduler: No tasks. Exiting...\n");
		exit(1);
	}
	CurrentL=Ql;
	Current=CurrentL;
	CurrentH = NULL;
	kill(Current->pid,SIGCONT);
	if(AliveProcesses!=1)
		alarm(SCHED_TQ_SEC);
	printf("Scheduler: Beginning scheduling\n");

	shell_request_loop(request_fd, return_fd);

	/* Now that the shell is gone, just loop forever
	 * until we exit from inside a signal handler.
	 */
	while (pause())
		;

	/* Unreachable */
	fprintf(stderr, "Internal error: Reached unreachable point\n");
	return 1;
}
