#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <assert.h>

#include <sys/wait.h>
#include <sys/types.h>

#include "proc-common.h"
#include "request.h"

/* Compile-time parameters. */
#define SCHED_TQ_SEC 2                /* time quantum */
#define TASK_NAME_SZ 60               /* maximum size for a task's name */

struct PCB{
	pid_t pid;//process pid
	int num;//process number
	char task_name[TASK_NAME_SZ];//name of the process
	struct PCB* next;
};

typedef struct PCB PCB;
PCB *Current,*temp;//o temp dixni tn proigumeni diadikasia-node  p imastan
int AliveProcesses;





/*
 * SIGALRM handler
 */
static void
sigalrm_handler(int signum)
{	
	int p=kill(Current->pid,SIGSTOP);
	if(p < 0){
		perror("SIGSTOP ERROR\n");	//alios stile stin energi tora diergasia sigstop na stamatisi
		exit(1);}
	printf("Scheduler: Received SIGALARM,process with PID=%d,is stopped by SIGSTOP\n",Current->pid);

}	




/* 
 * SIGCHLD handler
 */
static void
sigchld_handler(int signum)
{//piani t sigchild ite an estila sigstop o alarm handler ,ite an teliose mia diadikasia.
	pid_t p;
	int status;
	
	for(;;){	
		p = waitpid(-1, &status, WUNTRACED | WNOHANG);
		if (p < 0) {
			perror("waitpid");
			exit(1);
		}
		if (p == 0)
			break;
	
		

		if (WIFEXITED(status) || WIFSIGNALED(status)) {
			/* A child has died */
			alarm(0);
			AliveProcesses--;
			if(AliveProcesses!=0){
			
			temp->next=Current->next;
			printf("Scheduler: Received SIGCHLD, child is dead. Exiting.\n");
			free(Current);
			
				Current=temp->next;
				kill(Current->pid,SIGCONT);
				if(AliveProcesses!=1)
					alarm(SCHED_TQ_SEC);

					printf("Scheduler: Starting new process with PID=%d\n",Current->pid);
					
			}
			else{			
				printf("Scheduler: no more processes are alive-> Exiting.\n");
				exit(1);
			}
			
		} 
		 if (WIFSTOPPED(status)) {
			/* A child has stopped due to SIGSTOP/SIGTSTP, etc... */
			//printf("Scheduler:Received SIGCHLD, child process with PID =%d has been stopped\n",Current->pid);	
			temp=Current;
			Current=Current->next;				
			kill(Current->pid,SIGCONT);
			alarm(SCHED_TQ_SEC);
			printf("Scheduler: Starting new process with PID=%d\n",Current->pid);
		
		}
	
	}

}




/* Install two signal handlers.
 * One for SIGCHLD, one for SIGALRM.
 * Make sure both signals are masked when one of them is running.
 */
static void
install_signal_handlers(void)
{
	sigset_t sigset;
	struct sigaction sa;

	sa.sa_handler = sigchld_handler;
	sa.sa_flags = SA_RESTART;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGCHLD);
	sigaddset(&sigset, SIGALRM);
	sa.sa_mask = sigset;
	if (sigaction(SIGCHLD, &sa, NULL) < 0) {
		perror("sigaction: sigchld");
		exit(1);
	}

	sa.sa_handler = sigalrm_handler;
	if (sigaction(SIGALRM, &sa, NULL) < 0) {
		perror("sigaction: sigalrm");
		exit(1);
	}

	/*
	 * Ignore SIGPIPE, so that write()s to pipes
	 * with no reader do not result in us being killed,
	 * and write() returns EPIPE instead.
	 */
	if (signal(SIGPIPE, SIG_IGN) < 0) {
		perror("signal: sigpipe");
		exit(1);
	}
}


void child(char *task_name){
	char *newargv[] = { task_name, NULL, NULL, NULL };
	char *newenviron[] = { NULL };

	printf("I am %s, we are in the child's process with PID = %ld\n",
		task_name, (long)getpid());
	raise(SIGSTOP);

	execve(task_name, newargv, newenviron);

	/* execve() only returns on error */
	perror("execve");
	exit(1);

}



int main(int argc, char *argv[])
{
	int nproc,i;
	pid_t pid;
	PCB *Q=NULL;
	nproc =argc-1; /* number of proccesses goes here */
	/*
	 * For each of argv[1] to argv[argc - 1],
	 * create a new child process, add it to the process list.
	 */
	if (nproc == 0) {
		fprintf(stderr, "Scheduler: No tasks. Exiting...\n");
		exit(1);
	}
	
	AliveProcesses=nproc;

	for(i=0;i<nproc;i++){

			pid=fork();
			if(pid==0){	//if we are in child's process call the child function.
				child(argv[i+1]);
				exit(1);
			}
			if (pid==-1)
				perror("error in creating child process\n");
			if(pid>0){
				if(i==0){
				Q=(PCB*)malloc(sizeof(PCB));
				Q->pid=pid;
				Q->num=i+1;
				strcpy(Q->task_name,argv[i+1]);
				Q->next=NULL;
				Current=Q;
				}
				else {
					
				temp=(PCB*)malloc(sizeof(PCB));
				temp->pid=pid;
				temp->num=i+1;
				strcpy(temp->task_name,argv[i+1]);
	
				if(i==nproc-1)
				temp->next=Q;//to create the round robin we create a circular linked list
				else temp->next=NULL;
				
				Current->next=temp;
				Current=temp;
				}
			}					
	}

	/* Wait for all children to raise SIGSTOP before exec()ing. */
	wait_for_ready_children(nproc);

	/* Install SIGALRM and SIGCHLD handlers. */
	install_signal_handlers();
	Current=Q;
	kill(Current->pid,SIGCONT);
	if(AliveProcesses!=1)
		alarm(SCHED_TQ_SEC);
	printf("Scheduler: Beginning scheduling\n");

	/* loop forever  until we exit from inside a signal handler. */
	while (pause())
		;

	/* Unreachable */
	fprintf(stderr, "Internal error: Reached unreachable point\n");
	return 1;
}

