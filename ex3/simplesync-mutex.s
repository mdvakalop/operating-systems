	.file	"simplesync.c"
	.comm	locker,40,32
	.section	.rodata
	.align 8
.LC0:
	.string	"About to increase variable %d times\n"
.LC1:
	.string	"Done increasing variable.\n"
	.text
	.globl	increase_fn
	.type	increase_fn, @function
increase_fn:
.LFB5:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	stderr(%rip), %rax
	movl	$10000000, %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf@PLT
	movl	$0, -16(%rbp)
	jmp	.L2
.L4:
	leaq	locker(%rip), %rdi
	call	pthread_mutex_lock@PLT
	testl	%eax, %eax
	jne	.L3
	movq	-8(%rbp), %rax
	movl	(%rax), %eax
	leal	1(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, (%rax)
	leaq	locker(%rip), %rdi
	call	pthread_mutex_unlock@PLT
	movl	%eax, -12(%rbp)
.L3:
	addl	$1, -16(%rbp)
.L2:
	cmpl	$9999999, -16(%rbp)
	jle	.L4
	movq	stderr(%rip), %rax
	movq	%rax, %rcx
	movl	$26, %edx
	movl	$1, %esi
	leaq	.LC1(%rip), %rdi
	call	fwrite@PLT
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5:
	.size	increase_fn, .-increase_fn
	.section	.rodata
	.align 8
.LC2:
	.string	"About to decrease variable %d times\n"
.LC3:
	.string	"Done decreasing variable.\n"
	.text
	.globl	decrease_fn
	.type	decrease_fn, @function
decrease_fn:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movq	%rdi, -24(%rbp)
	movq	-24(%rbp), %rax
	movq	%rax, -8(%rbp)
	movq	stderr(%rip), %rax
	movl	$10000000, %edx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf@PLT
	movl	$0, -16(%rbp)
	jmp	.L7
.L9:
	leaq	locker(%rip), %rdi
	call	pthread_mutex_lock@PLT
	testl	%eax, %eax
	jne	.L8
	movq	-8(%rbp), %rax
	movl	(%rax), %eax
	leal	-1(%rax), %edx
	movq	-8(%rbp), %rax
	movl	%edx, (%rax)
	leaq	locker(%rip), %rdi
	call	pthread_mutex_unlock@PLT
	movl	%eax, -12(%rbp)
.L8:
	addl	$1, -16(%rbp)
.L7:
	cmpl	$9999999, -16(%rbp)
	jle	.L9
	movq	stderr(%rip), %rax
	movq	%rax, %rcx
	movl	$26, %edx
	movl	$1, %esi
	leaq	.LC3(%rip), %rdi
	call	fwrite@PLT
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	decrease_fn, .-decrease_fn
	.section	.rodata
.LC4:
	.string	"pthread_create"
.LC5:
	.string	"pthread_join"
.LC6:
	.string	""
.LC7:
	.string	"NOT "
.LC8:
	.string	"%sOK, val = %d.\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$64, %rsp
	movl	%edi, -52(%rbp)
	movq	%rsi, -64(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$0, -36(%rbp)
	leaq	-36(%rbp), %rdx
	leaq	-24(%rbp), %rax
	movq	%rdx, %rcx
	leaq	increase_fn(%rip), %rdx
	movl	$0, %esi
	movq	%rax, %rdi
	call	pthread_create@PLT
	movl	%eax, -32(%rbp)
	cmpl	$0, -32(%rbp)
	je	.L12
	call	__errno_location@PLT
	movq	%rax, %rdx
	movl	-32(%rbp), %eax
	movl	%eax, (%rdx)
	leaq	.LC4(%rip), %rdi
	call	perror@PLT
	movl	$1, %edi
	call	exit@PLT
.L12:
	leaq	-36(%rbp), %rdx
	leaq	-16(%rbp), %rax
	movq	%rdx, %rcx
	leaq	decrease_fn(%rip), %rdx
	movl	$0, %esi
	movq	%rax, %rdi
	call	pthread_create@PLT
	movl	%eax, -32(%rbp)
	cmpl	$0, -32(%rbp)
	je	.L13
	call	__errno_location@PLT
	movq	%rax, %rdx
	movl	-32(%rbp), %eax
	movl	%eax, (%rdx)
	leaq	.LC4(%rip), %rdi
	call	perror@PLT
	movl	$1, %edi
	call	exit@PLT
.L13:
	movq	-24(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	pthread_join@PLT
	movl	%eax, -32(%rbp)
	cmpl	$0, -32(%rbp)
	je	.L14
	call	__errno_location@PLT
	movq	%rax, %rdx
	movl	-32(%rbp), %eax
	movl	%eax, (%rdx)
	leaq	.LC5(%rip), %rdi
	call	perror@PLT
.L14:
	movq	-16(%rbp), %rax
	movl	$0, %esi
	movq	%rax, %rdi
	call	pthread_join@PLT
	movl	%eax, -32(%rbp)
	cmpl	$0, -32(%rbp)
	je	.L15
	call	__errno_location@PLT
	movq	%rax, %rdx
	movl	-32(%rbp), %eax
	movl	%eax, (%rdx)
	leaq	.LC5(%rip), %rdi
	call	perror@PLT
.L15:
	movl	-36(%rbp), %eax
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	movl	%eax, -28(%rbp)
	movl	-36(%rbp), %edx
	cmpl	$0, -28(%rbp)
	je	.L16
	leaq	.LC6(%rip), %rax
	jmp	.L17
.L16:
	leaq	.LC7(%rip), %rax
.L17:
	movq	%rax, %rsi
	leaq	.LC8(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	-28(%rbp), %eax
	movq	-8(%rbp), %rcx
	xorq	%fs:40, %rcx
	je	.L19
	call	__stack_chk_fail@PLT
.L19:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 7.2.0-8ubuntu3) 7.2.0"
	.section	.note.GNU-stack,"",@progbits
